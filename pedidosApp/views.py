from django.core.exceptions import ValidationError
from django.forms import fields
from django.forms.forms import Form
from django.http import response
from django.shortcuts import render
import requests
from .forms import PedidosForm
from .models import Pedidos
# Instanciamos las vistas genéricas de Django 
from django.views.generic import ListView, DetailView 
from django.views.generic.edit import UpdateView, DeleteView
# Nos sirve para redireccionar despues de una acción revertiendo patrones de expresiones regulares 
from django.urls import reverse
# Habilitamos el uso de mensajes en Django
from django.contrib import messages 
# Habilitamos los mensajes para class-based views 
from django.contrib.messages.views import SuccessMessageMixin 
 
# Habilitamos los formularios en Django
from django.contrib.auth.views import LoginView    


def inicio(request):
    form = PedidosForm(request.POST or None)
    temp_titulo = "Pedí tu prestamo!"
    if form.is_valid():        
        form_data = form.cleaned_data
        dni = form_data.get("dni")
        nombre = form_data.get("nombre")
        apellido = form_data.get("apellido")
        genero = form_data.get("genero")
        email = form_data.get("email")
        monto = form_data.get("monto")

        DataOK = True
        if dni < 0 or monto < 0:
            DataOK = False                

        #Consumo la API para la aprobación del prestamo
        url = 'https://api.moni.com.ar/api/v4/scoring/pre-score/'+ str(dni)
        response = requests.get(url, headers= {"credential":"ZGpzOTAzaWZuc2Zpb25kZnNubm5u"})       
        rj = response.json() 
        status = rj["status"]        
        
        #Si la respuesta de la API es OK grabo el pedido
        if response.status_code == 200 and status == "approve" and DataOK:
            obj = Pedidos()
            obj.dni = dni
            obj.nombre = nombre
            obj.apellido = apellido
            obj.genero = genero
            obj.email = email
            obj.monto = monto
            obj.save()

            messages.success(request, "Prestamo aprobado!")
        else:                        
            if DataOK:
                messages.error(request, "Prestamo rechazado!")
            else:
                messages.error(request, "Prestamo rechazado por datos inválidos!")                            

    context = {
        "temp_titulo": temp_titulo,
        "form": form,
    }    

    return render(request, "inicio.html", context)

class PedidosListado(ListView):
    model = Pedidos


class PedidosActualizar(SuccessMessageMixin, UpdateView):
    model = Pedidos
    form = Pedidos 
    #no muestro nunca el ID del prestamo
    fields = ["dni", "nombre", "apellido", "genero", "email", "monto"]    
    def get_success_url(self):
        return reverse('crud')


class PedidosEliminar(SuccessMessageMixin, DeleteView):
    model = Pedidos
    form = Pedidos
    fields = "__all__"

    def get_success_url(self):        
        return reverse('crud')


def login(request):
    return render(request, "login.html", {})