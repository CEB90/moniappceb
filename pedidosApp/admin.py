from pedidosApp.models import Pedidos
from django.contrib import admin
from .forms import PedidosModelForm

# Register your models here.
class AdminPedidos(admin.ModelAdmin):
    list_display = ["dni", "nombre", "apellido", "genero", "email", "monto", "timestamp"]
    form = PedidosModelForm
    list_filter = ["dni", "nombre", "apellido"]
    #class Meta:
    #    model = Pedidos


admin.site.register(Pedidos, AdminPedidos)