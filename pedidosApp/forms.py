from django import forms
from django.forms import fields
from django.forms.widgets import CheckboxSelectMultiple, ChoiceWidget
from .models import Pedidos

class PedidosModelForm(forms.ModelForm):
    class Meta:
        model = Pedidos
        fields = ["dni", "email", "monto"]
    


class PedidosForm(forms.Form):
    
    genero_choices = (        
        ('Masculino', 'Masculino'),
        ('Femenino', 'Femenino'),
        ('Otro', 'Otro')
    )
    
    dni = forms.IntegerField()
    nombre = forms.CharField(max_length=50)
    apellido = forms.CharField(max_length=50)
    genero = forms.ChoiceField(choices=genero_choices)
    email = forms.EmailField()
    monto = forms.FloatField()
